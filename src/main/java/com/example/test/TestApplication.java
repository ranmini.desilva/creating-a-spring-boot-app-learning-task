package com.example.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}
}

@RestController
@RequestMapping("/")
class MsgController{
	@Value("${greeting.message:Hello}") //default value = "Hello"
	private String greetingMessage;

	@GetMapping("/hello")
	public String hello(){
		return greetingMessage;
	}

	@GetMapping("/hello-world")
	public String helloWorld(){
		return "Hello World";
	}

}
