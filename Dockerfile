FROM openjdk:17-jdk-buster 
WORKDIR /app
COPY target/test-0.0.1-SNAPSHOT.jar .
CMD ["java", "-jar", "test-0.0.1-SNAPSHOT.jar"]
